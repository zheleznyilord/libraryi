﻿using LibraryI.Models;
using LibraryI.Models.ViewModels;
using LibraryI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace LibraryI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private ILibraryService service;
        private readonly IAccountService accountService;
        private ILogger<UserController> logger;
        private EmailService emailService;

        public UserController(ILibraryService service, IAccountService accountService, ILogger<UserController> logger,
            EmailService emailService)
        {
            this.service = service;
            this.accountService = accountService;
            this.logger = logger;
            this.emailService = emailService;
        }
        /// <summary>
        /// Get info about user
        /// </summary>
        /// <returns>All information about user</returns>
        [HttpGet("me")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> getInfoAsync()
        {
            //logger.LogInformation("user watch to himself");
            var user = await accountService.getUserAsync(User.Identity.Name);
            return Ok(new UserViewModel(user));
        }
        /// <summary>
        /// edit user
        /// </summary>
        /// <param name="user">User with changed fields</param>
        /// <returns></returns>
        [HttpPut("me")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public IActionResult editMe(User user)
        {
            service.editUser(user);
            return Ok();
        }

        [HttpGet("send")]
        public IActionResult send(string email = "ZheleznyiLord@gmail.com")
        {
            string textFromFile;
            using (FileStream fileStream = System.IO.File.OpenRead("Images/Congrats/Congrats.html"))
            {
                byte[] array = new byte[fileStream.Length];
                fileStream.Read(array, 0, array.Length);
                textFromFile = Encoding.Default.GetString(array);
            }

            string message = textFromFile;
            emailService.SendEmailAsync(email, "Tyyyy", message).GetAwaiter().GetResult();
            return Ok();
        }



    }
}
