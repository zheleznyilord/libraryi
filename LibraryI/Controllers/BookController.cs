﻿using System.Collections.Generic;
using AutoMapper;
using LibraryI.Models;
using LibraryI.Models.BookModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LibraryI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private ILibraryService libraryService;

        public BookController(ILibraryService libraryService)
        {
            this.libraryService = libraryService;
        }
        /// <summary>
        /// Admin can create book
        /// </summary>
        /// <param name="bookCreateModel">Input tittle, athor, quantity and topics</param>
        /// <returns></returns>
        [Authorize(AuthenticationSchemes = "Bearer",Roles ="admin")]
        [HttpPost("create")]
        public IActionResult returnBook(BookCreateModel bookCreateModel)
        {
            var book = Mapper.Map<BookCreateModel, Book>(bookCreateModel);
            var b = libraryService.createBook(book);
            libraryService.createCategory(bookCreateModel.CategoryNames, b.Id);
            return Ok();
        }
        /// <summary>
        /// Get all books with pagination and filtration by author, tittle or category
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// <response code="200">Succesffully returned books</response>
        /// <response code="400"></response>
        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPost("getbooks")]
        public IActionResult books(BookPageModel model)
        {
            var (count, books) = libraryService.getAllBooks(model);
            var source = Mapper.Map<IEnumerable<Book>, List<BookViewModel>>(books);

            PageViewModel pageViewModel = new PageViewModel(count, model.Page, model.PageSize);
            IndexViewModel viewModel = new IndexViewModel
            {
                PageViewModel = pageViewModel,
                Books = source
            };
            return Ok(viewModel);
        }
        /// <summary>
        /// User can take book by Id
        /// </summary>
        /// <param name="bookId"></param>
        /// <returns></returns>
        /// <response code="200">User take book succesfully</response>
        /// <response code="400">Library does not have this book or user have book</response>
        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPut]
        public IActionResult books(int bookId)
        {
            var status = libraryService.takeBook(bookId, User.Identity.Name);
            if (status)
            {
                return Ok();
            }
            throw new BookException("Library does not have this book or user have this book");
        }
        /// <summary>
        /// Returns book to library, only if you have book
        /// </summary>
        /// <returns></returns>
        /// <response code="200">User return book succesfully</response>
        /// <response code="400">If user doesnt have book</response>
        [Authorize(AuthenticationSchemes = "Bearer")]
        [HttpPut("return")]
        public IActionResult returnBook()
        {
            libraryService.returnBook(User.Identity.Name);
            return Ok();
        }
        /// <summary>
        /// Delete book by Id, only for admin
        /// </summary>
        /// <remarks>
        ///  id: 1
        /// </remarks>
        /// <param name="bookId">Id book, what you want delete</param>
        /// <returns></returns>
        /// <response code="204">Deleted succesfully</response>
        /// <response code="400">If id incorrect</response>
        [Authorize(AuthenticationSchemes = "Bearer",Roles = "admin")]
        [HttpDelete]
        public IActionResult deleteBook(int bookId)
        {
            libraryService.deleteBook(bookId);
            return NoContent();
        }
    }
}