﻿using LibraryI.Models;
using LibraryI.Models.ViewModels;
using LibraryI.Recovery;
using LibraryI.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace LibraryI.Views
{
    [ApiController]
    [Route("api/[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly SignInManager<User> _signInManager;
        private readonly IAccountService accountService;
        private readonly Microsoft.AspNetCore.Identity.UserManager<User> userManager;
        private EmailService emailService;

        public AccountController(SignInManager<User> signInManager, IAccountService accountService, Microsoft.AspNetCore.Identity.UserManager<User> userManager,
            EmailService emailService)
        {
            _signInManager = signInManager;
            this.accountService = accountService;
            this.userManager = userManager;
            this.emailService = emailService;
        }
         
        /// <summary>
        /// Login for users
        /// </summary>
        /// <param name="model">Input login and password</param>
        /// <returns></returns>
        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var identity = await accountService.getTokenAsync(model);
                var encodedJwt = accountService.getJwt(identity);
                var response = new
                {
                    access_token = encodedJwt,
                    username = identity.Name
                };

                return Ok(response);
            }
            return Ok(model);
        }

        /// <summary>
        /// Logout for users
        /// </summary>
        /// <returns></returns>
        [HttpPost("logOff")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await _signInManager.SignOutAsync();
            return Ok();
        }
        /// <summary>
        /// get info about user by Id
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("getUser")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> getInfoAsync(string userId)
        {
            var user = await accountService.getUserByIdAsync(userId);
            return Ok(new UserViewModel(user));
        }
        /// <summary>
        /// registration for new users
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("registration")]
        public async Task<IActionResult> registrationAsync(RegisterViewModel model)
        {
            var result = await accountService.addUserAsync(model);
            var callbackUrl = Url.Action(
                    "ConfirmEmail",
                    "Account",
                    new { userId = result.Item1, code = result.Item2 },
                    protocol: HttpContext.Request.Scheme);

            await emailService.SendEmailAsync(model.Email, "Confirm your account",
                $"Подтвердите регистрацию, перейдя по ссылке: <a href='{callbackUrl}'>link</a>");
            return Ok(result);
        }

        [HttpGet("confirmEmail")]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            var result = accountService.confirmEmailAsync(userId, code);
            if (result.Result.Succeeded)
                return Ok();
            else
                return BadRequest();
        }

        /// <summary>
        /// delete users for admin
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpDelete("delete")]
        [Authorize(AuthenticationSchemes = "Bearer",Roles ="admin")]
        public async Task<IActionResult> deleteUserAsync(string userId)
        {
            await accountService.deleteUserAsync(userId);
            return NoContent();
        }

        [HttpPost("forgot")]
        [AllowAnonymous]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var token = accountService.recoveryPassword(model);
                var callbackUrl =
                   Url.Action("ResetPassword", "Account", new { token = token },
                   protocol: HttpContext.Request.Scheme);
                string message = $"Для сброса пароля пройдите по ссылке: <a href='{callbackUrl}'>link</a>";
                await emailService.SendEmailAsync(model.Email, "recovery", message);
                return Ok(token);
            }
            return BadRequest("Email is not valid!");
        }

        [HttpGet("reset")]
        [AllowAnonymous]
        public IActionResult ResetPassword(string token = null)
        {         
            return Redirect($"https://google.com?token={token}");
        }

        [HttpPost("reset")]
        [AllowAnonymous]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var result = await accountService.changePasswordAsync(model);          
            return Ok();
        }

    }
}