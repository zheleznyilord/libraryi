﻿using System.Collections.Generic;

namespace LibraryI
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        T GetById(object id);
        void Insert(T obj);
        void InsertRange(List<T> obj);
        void Update(T obj);
        void Delete(object id);
        void Save();
    }
}
