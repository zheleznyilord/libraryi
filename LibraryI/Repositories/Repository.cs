﻿using LibraryI.Exceptions;
using LibraryI.Models.BookModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LibraryI.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private BookContext _context = null;
        private DbSet<T> table = null;
        public Repository(BookContext bookContext)
        {
            this._context = bookContext;
            table = _context.Set<T>();
        }

        public IEnumerable<T> GetAll()
        {
            return table.ToList();
        }
        public T GetById(object id)
        {
            return table.Find(id);
        }
        public void Insert(T obj)
        {
            table.Add(obj);
            Save();
        }
        public void InsertRange(List<T> obj)
        {
            table.AddRange(obj);
            Save();
        }
        public void Update(T obj)
        {
            table.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
            Save();
        }
        public void Delete(object id)
        {
            T existing = table.Find(id);
            table.Remove(existing);
            Save();
        }
        public void Save()
        {
            try
            {
                _context.SaveChanges();
            } catch(Exception e)
            {
                throw new DbException("Cant save database"+ e.Message);
            }
        }
    }
}
