﻿using LibraryI.Models.BookModel;
using System.Collections.Generic;

namespace LibraryI.Models
{
    public class IndexViewModel
    {
        public IEnumerable<BookViewModel> Books { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}
