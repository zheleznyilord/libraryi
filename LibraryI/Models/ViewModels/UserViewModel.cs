﻿using System.Collections.Generic;

namespace LibraryI.Models.ViewModels
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public int? BookId { get; set; }
        public List<OldBook> OldBooks { get; set; }

        public UserViewModel()
        {

        }

        public UserViewModel(User user)
        {
            Id = user.Id;
            Email = user.Email;
            Name = user.Name;
            BookId = user.BookId;
            OldBooks = user.OldBooks;
        }
    }
}
