﻿using System.Collections.Generic;
using LibraryI.Models.BookModel;
using Microsoft.AspNetCore.Identity;

namespace LibraryI.Models
{
    public class User : IdentityUser
    {
        public string Name { get; set; }
     
        public int? BookId { get; set; }
        public Book Book { get; set; }
        public List<OldBook> OldBooks { get; set; }
    }
}
