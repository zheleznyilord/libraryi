﻿namespace LibraryI.Models.BookModel
{
    public enum BookSortState
    {
        TITTLEASC ,
        TITTLEDESC,
        AUTHORASC,
        AUTHORDESC,
        QUANTITYASC,
        QUANTITYDESC

    }
}
