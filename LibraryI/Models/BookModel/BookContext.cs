﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace LibraryI.Models.BookModel
{
    public class BookContext : IdentityDbContext<User>
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Category> Categories { get; set; }

        public DbSet<OldBook> OldBook { get; set; }

        public BookContext(DbContextOptions<BookContext> options)
            : base(options)
        { 
            Database.EnsureCreated();
        }
        
        public BookContext()
        {
        }
    }
}
