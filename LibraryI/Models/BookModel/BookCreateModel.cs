﻿using System.Collections.Generic;

namespace LibraryI.Models.BookModel
{
    public class BookCreateModel
    {
        public string Tittle { get; set; }
        public string Author { get; set; }
        public int Quantity { get; set; }
        public List<string> CategoryNames { get; set; }


    }
}
