﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LibraryI.Models.BookModel
{
    public class Category
    { 
        public int CategoryId { get; set; }
        [Required]
        public string CategoryName { get; set; }
        public int BookId { get; set; }
        public Book Book { get; set; }
               

        public override bool Equals(Object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Category p = (Category)obj;
                return (CategoryName.Equals(p.CategoryName)) || (CategoryId == p.CategoryId);
            }
        }

        public override int GetHashCode() => CategoryName.GetHashCode() * CategoryId;
       
    }
}
