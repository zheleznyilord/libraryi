﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LibraryI.Models.BookModel
{
    public class Book
    {
        public int Id { get; set; }
        [Required]
        public string Tittle { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        [Range(0, 99,ErrorMessage = "Quantity books must be from 0 to 99")]
        public int Quantity { get; set; }
        [Required]
        public List<Category> Categories { get; set; }

    }
}