﻿namespace LibraryI.Models.BookModel
{
    public class BookPageModel
    {
        public string Tittle { get; set; }
        public string Author { get; set; }
        public string CategoryName { get; set; }
        public int Page {get;set;}
        public int PageSize {get; set;}
        public BookSortState SortState { get; set; }

        public BookPageModel()
        {
            Page = 1;
            PageSize = 2;
            SortState = BookSortState.TITTLEASC;
        }

    }
}
