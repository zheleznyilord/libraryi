﻿using System.Collections.Generic;

namespace LibraryI.Models.BookModel
{
    public class BookViewModel
    {
        public int Id { get; set; }
        public string Tittle { get; set; }
        public string Author { get; set; }
        public int Quantity { get; set; }
        public List<Category> Categories { get; set; }


    }
}
