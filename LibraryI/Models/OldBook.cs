﻿using LibraryI.Models.BookModel;

namespace LibraryI.Models
{
    public class OldBook
    {
        public int Id { get; set; }
        public int BookId { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
        public Book Book { get; set; }
    }
}