﻿using LibraryI.Models;
using LibraryI.Models.BookModel;
using System.Collections.Generic;

namespace LibraryI
{
    public interface ILibraryService
    {
        (int, List<Book>) getAllBooks(BookPageModel model);
        Book createBook(Book book);
        void editUser(User user);
        bool takeBook(int bookId, string userName);
        void returnBook(string userName);
        void deleteBook(int bookId);
        void createCategory(List<string> categoryNames, int id);
    }
}