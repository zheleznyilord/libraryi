﻿using Microsoft.Extensions.Hosting;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace LibraryI.Services
{
    internal class TimedHostedService : IHostedService, IDisposable
    {
        
        private Timer _timer;

        public TimedHostedService()
        {
            
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {

            _timer = new Timer(DoWorkAsync,
                               null,
                               TimeSpan.Zero,
                               TimeSpan.FromSeconds(60));

            return Task.CompletedTask;
        }

        private void DoWorkAsync(object state)
        {
            EmailService email = new EmailService();

            email.SendEmailAsync("zheleznyilord@gmail.com", "subject2", "text2222222222222").GetAwaiter().GetResult();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {

            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
