﻿using LibraryI.Exceptions;
using LibraryI.Models;
using LibraryI.Models.BookModel;
using LibraryI.Models.ViewModels;
using LibraryI.Recovery;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Routing;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Policy;
using System.Threading.Tasks;

namespace LibraryI.Services
{
    public class AccountService : IAccountService
    {
        private UserManager<User> _userManager;
        private BookContext bookContext;
        private LinkGenerator linkGenerator;

        public AccountService(UserManager<User> userManager, BookContext bookContext, LinkGenerator linkGenerator)
        {
            this._userManager = userManager;
            this.bookContext = bookContext;
            this.linkGenerator = linkGenerator;
        }

        public async Task<ClaimsIdentity> getTokenAsync(LoginViewModel model)
        {
            var identity = await GetIdentity(model.Email, model.Password);
            if (identity == null)
            {
                throw new AccountException("incorrect password or login");
            }
            return identity;
        }

        public string getJwt(ClaimsIdentity  identity)
        {
            var now = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: now,
                    claims: identity.Claims,
                    expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;

        }

        private async Task<ClaimsIdentity> GetIdentity(string username, string password)
        {
            var userToVerify = await _userManager.FindByEmailAsync(username);
            var result = await _userManager.CheckPasswordAsync(userToVerify, password);
            if (result)
            {
                User person = _userManager.Users.FirstOrDefault(user => user.Email == username);
                if (person != null)
                {
                    var user = _userManager.FindByEmailAsync(username);
                    var roles = await _userManager.GetRolesAsync(user.Result);
                    var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, username),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, roles.First())
                };
                    ClaimsIdentity claimsIdentity =
                    new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType,
                        ClaimsIdentity.DefaultRoleClaimType);
                    return claimsIdentity;
                }
            }

            return null;
        }

        public async Task deleteUserAsync(string userId)
        {
            var user = _userManager.FindByIdAsync(userId);
            await _userManager.DeleteAsync(user.Result);
        }


        public async Task<User> getUserByIdAsync(string userId)
        {
            return await _userManager.FindByIdAsync(userId);
        }

        public async Task<(string, string)> addUserAsync(RegisterViewModel model)
        {
            User user = new User { Email = model.Email, UserName = model.Email };
            var result = await _userManager.CreateAsync(user, model.Password);
            await _userManager.AddToRoleAsync(user, "admin");
            if (result.Succeeded)
            {
                var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                return (user.Id, code);
            }
            return ("","");
        }
        public async Task<User> getUserAsync(string name)
        {
            var user = await _userManager.FindByEmailAsync(name);
            var list = bookContext.OldBook.Where(o => o.UserId.Equals(user.Id)).ToList();
            return user;
        }

        public string recoveryPassword(ForgotPasswordViewModel model)
        {
            var user = _userManager.FindByEmailAsync(model.Email);
            var token = _userManager.GeneratePasswordResetTokenAsync(user.Result);
            return token.Result;
        }

        public async Task<IdentityResult> changePasswordAsync(ResetPasswordViewModel model)
        {
            var user = await _userManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                throw new AccountException("1");
            }
            var result = await _userManager.ResetPasswordAsync(user, model.Token, model.Password);
            if (!result.Succeeded)
            {
                throw new AccountException("2");
            }
            return result;
        }

        public async Task<IdentityResult> confirmEmailAsync(string userId, string code)
        {
            if (userId == null || code == null)
            {
                throw new AccountException("");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new AccountException("");
            }
            return await _userManager.ConfirmEmailAsync(user, code);
        }
    }
}
