﻿using LibraryI.Models;
using LibraryI.Models.BookModel;
using LibraryI.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace LibraryI
{
    public class LibraryService : ILibraryService
    {
        private BookContext db;
        private UserManager<User> userManager;
        private IRepository<Book> repo;
        private IRepository<Category> catRepo;

        public LibraryService(BookContext context, UserManager<User> userManager)
        {
            this.db = context;
            this.userManager = userManager;
            this.repo = new Repository<Book>(context);
            this.catRepo = new Repository<Category>(context);
        }

        public (int, List<Book>) getAllBooks(BookPageModel model)
        {
            var list = new List<Book>();
            int count = 0;
            if ((model.CategoryName != null && !model.CategoryName.Equals("")) || (model.Tittle!=null&&!model.Tittle.Equals("")) ||
                (model.Author != null && !model.Author.Equals("")))
            {

                var (c, bookss) = getBooks(model);
                count = c;
                list.AddRange(bookss);
            }
            IQueryable<Book> books;
            if (list.Count == 0)
            {
                var dbBooks = db.Books;
                count = dbBooks.Count();
                books = switchBooks(model.SortState, dbBooks).Skip((model.Page-1)*model.PageSize).Take(model.PageSize).Include(x => x.Categories);
            }
            else
            {
                books = list.AsQueryable();
            }           
            return (count, books.ToList());
        }

        public void createCategory(List<string> categoryNames, int id)
        {
            var cats = new List<Category>();
            foreach(var cat in categoryNames)
            {
                cats.Add(new Category { BookId = id, CategoryName = cat });
            }
            catRepo.InsertRange(cats);
        }

        public Book createBook(Book book)
        {
            repo.Insert(book);
            return db.Books.Where(b => b.Tittle.Equals(book.Tittle) && b.Author.Equals(book.Author)).First();
        }

        private IQueryable<Book> switchBooks(BookSortState sortState, DbSet<Book> books)
        {
            switch (sortState)
            {
                case BookSortState.TITTLEASC:
                    return books.OrderBy(b => b.Tittle);
                case BookSortState.TITTLEDESC:
                    return books.OrderByDescending(b => b.Tittle);
                case BookSortState.AUTHORASC:
                    return books.OrderBy(b => b.Author);
                case BookSortState.AUTHORDESC:
                    return books.OrderByDescending(b => b.Author);
                case BookSortState.QUANTITYASC:
                    return books.OrderBy(b => b.Quantity);
                case BookSortState.QUANTITYDESC:
                    return books.OrderByDescending(b => b.Quantity);
            }
            return null;
        }

        public void editUser(User user)
        {
            userManager.UpdateAsync(user);
        }

        private (int , List<Book>) getBooks(BookPageModel model)
        {
            var dbBooks = db.Books;
            var books = switchBooks(model.SortState, dbBooks).
                Include(x => x.Categories).Where(b => b.Tittle.Contains(model.Tittle) ||
              b.Author.Contains(model.Author) || b.Categories.Any(c => c.CategoryName.Contains(model.CategoryName)));
            var count = books.Count();
            var newBooks = books.Skip((model.Page - 1)*model.PageSize).Take(model.PageSize);
            var tuple = (count, newBooks.ToList());
            return tuple;
        }
        
        public bool takeBook(int bookId, string userName)
        {
            var status = minusCount(bookId);
            if (status)
            { 
                var user = db.Users.Where(u => u.Email.Equals(userName)).First();
                if (user.BookId != null)
                {
                    if (user.BookId != 0)
                    {
                        throw new BookException("User have book");
                    }
                }
                user.BookId = bookId;
                db.Update(user);
                db.SaveChanges();
            }
            return status;
        }

        private bool minusCount(int id)
        {
            var book = db.Books.FirstOrDefault(x=>x.Id == id);
            if (book.Quantity < 1)
            {
                throw new BookException("Library does not have this book");
            }
            book.Quantity--;
            db.Update(book);
            db.SaveChanges();
            return true;
        }

        public void returnBook(string userName)
        {
            var user = db.Users.First(u => u.Email.Equals(userName));
            if (user.BookId == null || user.BookId == 0)
            {
                throw new BookException("User doesnt have book");
            }
            var book = db.Books.Where(b => b.Id == user.BookId).First();
            var cat = db.Categories.Where(c => c.BookId == book.Id).First();
            book.Quantity++;
            db.Update(book);
            db.SaveChanges();
            var ob = new OldBook { BookId = (int)user.BookId, UserId = user.Id };
            db.Add(ob);
            db.SaveChanges();
            user.BookId = null;
            user.Book = null;
            db.Update(user);
            db.SaveChanges();
        }

        public void deleteBook(int bookId)
        {
            repo.Delete(bookId);
        }
    }
}
 