﻿using LibraryI.Models;
using LibraryI.Models.ViewModels;
using LibraryI.Recovery;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LibraryI.Services
{
    public interface IAccountService
    {
        Task<ClaimsIdentity> getTokenAsync(LoginViewModel model);
        string getJwt(ClaimsIdentity identity);
        Task deleteUserAsync(string userId);
        Task<User> getUserByIdAsync(string userId);
        Task<(string, string)> addUserAsync(RegisterViewModel model);
        Task<User> getUserAsync(string name);
        string recoveryPassword(ForgotPasswordViewModel model);
        Task<IdentityResult> changePasswordAsync(ResetPasswordViewModel model);
        Task<IdentityResult> confirmEmailAsync(string userId, string code);
    }
}
