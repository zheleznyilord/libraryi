﻿using LibraryI.Models;
using LibraryI.Models.BookModel;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryI
{
    public static class SampleData
    {
        public static async Task Initialize(BookContext context, UserManager<User> userManager, 
            RoleManager<IdentityRole> roleManager)
        {
            if (!context.Books.Any())
            {
                initializeBooks(context); 
            }
            if (!context.Categories.Any())
            {
                initializeCategories(context);
            }
            await InitializeUsersAsync(userManager, roleManager);           
        }

        private static async Task InitializeUsersAsync(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {

            string adminEmail = "admim@admin.com";
            string password = "Admin_123";
            if (await roleManager.FindByNameAsync("admin") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("admin"));
            }
            if (await roleManager.FindByNameAsync("user") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("user"));
            }
            if (await roleManager.FindByNameAsync("TL") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("TL"));
            }
            if (await userManager.FindByNameAsync(adminEmail) == null)
            {
                User admin = new User { Email = adminEmail, UserName = adminEmail };
                IdentityResult result = await userManager.CreateAsync(admin, password);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(admin, "admin");
                }
                var user = await userManager.FindByNameAsync(adminEmail);
                user.OldBooks = new List<OldBook> {
                    new OldBook { BookId = 1,UserId= "2a8353b4-1c1e-4015-9789-55642398aefa" },
                    new OldBook { BookId = 2,UserId= "2a8353b4-1c1e-4015-9789-55642398aefa" },
                    new OldBook { BookId = 1,UserId= "2a8353b4-1c1e-4015-9789-55642398aefa" }
                };
                await userManager.UpdateAsync(user);
            }
         
        }


        private static void initializeBooks(BookContext context)
        {
            context.Books.AddRange(
                new Book
                {
                    Tittle = "1984",
                    Author = "George Orwell",
                    Quantity = 15
                 },
                    new Book
                    {
                        Tittle = "Harry Potter and the Goblet of Fire",
                        Author = "Joanne Rowling",
                        Quantity = 11
                    },
                    new Book
                    {
                        Tittle = "Harry Potter and the Chamber of Secrets",
                        Author = "Joanne Rowling",
                        Quantity = 99
                    }
                );
                context.SaveChanges();           
        }
        private static void initializeCategories(BookContext context)
        {
            context.Categories.AddRange(
                new Category
                {
                    BookId = 1,
                    CategoryName ="fantasy"
                },
                new Category
                {
                    BookId = 1,
                    CategoryName = "dystopian novel"
                },
                new Category
                {
                    BookId = 2,
                    CategoryName = "fantasy"
                },
                new Category
                {
                    BookId = 3,
                    CategoryName = "fantasy"
                }
            );
            context.SaveChanges();
        }
    }
}
