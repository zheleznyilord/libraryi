﻿using System;

namespace LibraryI.Exceptions
{
    public class AccountException : Exception
    {

        public AccountException(string message)
        : base(message)
        {

        }
    }
}
