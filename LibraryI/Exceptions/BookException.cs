﻿using System;

namespace LibraryI.Models.BookModel
{

    public class BookException : Exception
    {

        public BookException(string message)
        : base(message)
        {

        }

    }
}
