﻿using System;

namespace LibraryI.Exceptions
{
    public class DbException : Exception
    {
        public DbException(string message) : base(message)
        {

        }
    }
}
