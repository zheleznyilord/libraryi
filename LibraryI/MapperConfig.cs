﻿using AutoMapper;
using LibraryI.Models.BookModel;

namespace LibraryI
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            CreateMap<Book, BookViewModel>();
            CreateMap<BookCreateModel, Book>();
        }
    }
}
